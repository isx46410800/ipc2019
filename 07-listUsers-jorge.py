#!/usr/bin/python3
# coding: utf-8

# https://docs.python.org/3.3/library/argparse.html?highlight=argparse
# https://docs.python.org/3.3/howto/argparse.html?highlight=argparse



class UnixUser():
    """ Classe unix user: prototipo de /etc/passwd
    login:passwd:uid:gid:gecos:home:shell"""
    
    def __init__(self, login, pas, uid, gid, gecos, home, shell):
        "constructor objetos UnixUser"
        self.login=login
        self.passwd=pas
        self.uid=uid
        self.gid=gid
        self.gecos=gecos
        self.home=home
        self.shell=shell
    
    def show(self):
        "Mostrar datos de usuario"
        print(f"login: {self.login} uid: {self.uid} gid: {self.gid} gecos: {self.gecos} home: {self.home} shell: {self.shell}")
    
    def changePasswd(self, passwd):
        self.passwd = passwd
    
    def changeHome(self, home):
        self.home = home

    def __str__(self):
        "Funcio per retornar string del objeto"
        return f"{self.login} {self.uid} {self.gid}"
        

# ejercicio 7 
# 07-list-users.py [-f file | default stdin] de /etc/passwd

# 1 populate cargar usuarios en una lista
# 2 recorrer esta lista y mostrarla


# coges una linea creas un objeto y lo añades a una lista

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--file", type=str, dest="fitcher",
                     default="/dev/stdin", metavar="file",
                     help="fichero a recorrer, por defecto stdin")
args=parser.parse_args()

print(args)

archivo = open(args.fitcher, 'r')
listUsers = []

# 1 populate cargar usuarios en una lista
for line in archivo:
    user = line.split(':')
    # quito el salto de linea si existe
    if user[6][-1:] == '\n' : user[6] = user[6][:-1]
    listUsers.append( UnixUser(user[0], user[1], user[2], user[3], user[4], user[5], user[6]) )

archivo.close()

# 2 recorrer esta lista y mostrarla
for user in listUsers:
    user.show()