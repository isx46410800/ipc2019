#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Donat un file tipus /etc/passwd o stdin (amb aquest format) fer:

#-el constructor d'objectes UnixUser rep un strg amb la línia sencera tipus /etc/passwd.
#-llegir línia a línia cada usuari assignant-lo a un objecte UnixUser i afegir l’usuari a una llista d’usuaris UnixUser.
#-un cop completada la carrega de dades i amb la llista amb tots els usuaris, llistar per pantalla els usuaris recorrent la llista.

import sys, argparse
parser = argparse.ArgumentParser(description="Iterar els usuaris del /etc/passwd", epilog="Fi del programa")
parser.add_argument("-f","--fit",type=str, help="file /etc/passwd o stdin", metavar="file", default="/dev/stdin",dest="fitxer")
args=parser.parse_args()
# -------------------------------------------------------
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,liniaUser):
    "Constructor objectes UnixUser"
    campos=liniaUser.split(":")
    self.login=campos[0]
    self.passwd=campos[1]
    self.uid=int(campos[2])
    self.gid=int(campos[3])
    self.gecos=campos[4]
    self.home=campos[5]
    self.shell=campos[6][:-1]
  def show(self):
    "Mostra les dades de l'usuari"
    print "login: %s, passwd: %s, uid: %d, gid: %d, gecos: %s, home: %s, shell: %s" % (self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)
  def __str__(self):
    "functió to_string"
    return "%s %s %d %d %s %s %s" % (self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)
# -------------------------------------------------------
fileIn=open(args.fitxer,"r")
userList=[]
for line in fileIn:
  user=UnixUser(line)
  userList.append(user)
fileIn.close()
for user in userList:
 print user
exit(0)


