# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 07-list-users.py [-f file (stdin)] --> Sempre /etc/passwd.
#  1. Populate --> Agafes linea, crees objecte (amb totes les dades, passwd empty), i guardes a la llista.
#  2. List --> Llista els usuaris de la llista
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------

#Definirem una classe, que defineixi el que per a nosaltres és un usuari UNIX,
# el nom comença amb Majúscules, si el mètode es __ és implícit
class UnixUser():
	"""Classe UnixUser: Prototipus de /etc/passwd
	login:passwd:uid:gid:gecos:home:shell"""
	def __init__(self, login, passwd, uid, gid, gecos, home, shell):
		"""Constructor d'objectes UnixUser"""
		self.login=login
		self.passwd=passwd
		self.uid=uid
		self.gid=gid
		self.gecos=gecos
		self.home=home
		self.shell=shell
	def __str__(self):
		"""Funció per retornar un string del objecte"""
		return "%s %s %d %d %s %s %s" %(self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)
	def show(self):
		"""Mostrar les dades de l'usuari"""
		print (f"Login:%s Passwd:%s Uid:%d Gid:%d Gecos:%s Home:%s Shell:%s" %(self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell),end="")

# Validem els arguments
import sys, argparse
parser = argparse.ArgumentParser(description=\
        """Genera una llista amb els usuaris de /etc/passwd """)
parser.add_argument("-f","--file",type=str, help="Fitxer a processar",dest="file",\
        metavar="file",default="/etc/passwd")
args=parser.parse_args()

llista_users=[]

fileIn=open(args.file,"r")
for user in fileIn:
	# Generem la llista amb tots els camps
	dades=(user.split(":"))
	
	# Guardem les dades
	login=(dades[0])
	passwd=(dades[1])
	uid=int((dades[2]))
	gid=int((dades[3]))
	gecos=(dades[4])
	home=(dades[5])
	shell=(dades[6])
	
	# Creem l'usuari i l'afegim a la llista
	login=UnixUser(login,passwd,uid,gid,gecos,home,shell)
	llista_users.append(login)
fileIn.close()

# Tenim dos metodes per a mostrar-ho
for login in llista_users:
	#print(login,end="")
	login.show()
	
