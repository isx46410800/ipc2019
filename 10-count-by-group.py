#!/usr/bin/python3
# -*- coding: utf-8 -*-
#10-count-by-group.py [-s gid | gname | nusers ] -u usuaris -g grups
#LListar els grups del sistema ordenats pel criteri de gname, gid o de número d'usuaris.
#Atenció cal gestionar apropiadament la duplicitat dels usuaris en un grup.
#Requeriment: desar a la llista d'usuaris del grup tots aquells usuaris que hi pertanyin, sense duplicitats, tant com a grup principal com a grup secundari.

# -------------------------------------------------------
#librerias importadas
import sys, argparse
parser = argparse.ArgumentParser(description="Iterar els usuaris del /etc/passwd", epilog="Fi del programa")
parser.add_argument("-s", "--sort", type=str, help="ordenacio per login, gid o gname", metavar="ordre", dest="ordenacio", choices=["nusers","gid", "gname"])
parser.add_argument("-u", "--userFile", required=True, type=str, help="file /etc/passwd", metavar="userFile")
parser.add_argument("-g", "--groupFile", required=True, type=str, help="file /etc/passwd", metavar="groupFile")
args=parser.parse_args()
# -------------------------------------------------------
#Definimos diccionario para el gname
groupsDict={}

# -------------------------------------------------------
#CLASE PARA EL /ETC/PASSWD
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,liniaUser):
    "Constructor objectes UnixUser"
    campos=liniaUser.split(":")
    self.login=campos[0]
    self.passwd=campos[1]
    self.uid=int(campos[2])
    self.gid=int(campos[3])
    #buscamos en el dicc segun gid(key) y el campo gname(valor) si está:
    self.gname=""
    if self.gid in groupsDict:
      self.gname=groupsDict[self.gid].gname
    self.gecos=campos[4]
    self.home=campos[5]
    self.shell=campos[6][:-1]
  def show(self):
    "Mostra les dades de l'usuari"
    print("login: %s, passwd: %s, uid: %d, gid: %d, gecos: %s, home: %s, shell: %s" % (self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell))
  def __str__(self):
    "funció to_string"
    return "%s %s %d %d %s %s %s" % (self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)
 
#CLASE PARA EL /ETC/GROUP
class UnixGroup():
  """Classe UnixUser: prototipus de /etc/group
  gname:passwd:gid:usersList"""
  def __init__(self,liniaGroup):
    "Constructor objectes UnixUser"
    campos=liniaGroup.split(":")
    self.gname=campos[0]
    self.passwd=campos[1]
    self.gid=int(campos[2])
    self.usersListStr = campos[3][:-1]
    self.usersList=[]
    if self.usersListStr:
      self.usersList = self.usersListStr.split(",")
  def show(self):
    "Mostra les dades del group"
    print("gname: %s, passwd: %s, gid: %d, usersList: %s" % (self.gname, self.passwd, self.gid, self.usersList))
  def __str__(self):
    "funció to_string"
    return "%s %s %d %s" % (self.gname, self.passwd, self.gid, self.usersList)
# -------------------------------------------------------
#------------------------------------------------------- 
#abrimos fichero /etc/group y añadimos clave gid y valores de lineGroup
groupFileIn=open(args.groupFile,"r")
for line in groupFileIn:
  group=UnixGroup(line)
  groupsDict[group.gid]=group
groupFileIn.close()
# --------------------------------------------------------
#abrimos fichero /etc/passwd y metemos cada linia de user en la lista
userFileIn=open(args.userFile,"r")
usersList=[]
for line in userFileIn:
  user=UnixUser(line)
  usersList.append(user)
  if user.gid in groupsDict:
	  if user.login not in groupsDict[user.gid].usersList:
		  groupsDict[user.gid].usersList.append(user.login)
userFileIn.close()
# -------------------------------------------------------
#Creamos una lista de indices segun sea por gid, gname o nusers groupsDict[gid]=gname,passwd,gid,users
index=[]

if args.ordenacio == "gname":
	index = [ (groupsDict[k].gname, k) for k in groupsDict ]
elif args.ordenacio == "nusers":
	index = [ (len(groupsDict[k].usersList), k) for k in groupsDict ]
else:
	index = [ k for k in groupsDict ]
##ordenamos estos index para tener la lista ordenada segun gid, gname o nusers
index.sort()
# -------------------------------------------------------	
#mostramos cada linea del groups segun su ordenacion
if args.ordenacio == "gname":
	for g,k in index:
		print(groupsDict[k])
elif args.ordenacio == "nusers":
	for nusers,k in index:
		print(groupsDict[k])
##por defecto sin opcion sea por gid
else:
	for k in index:
		print(groupsDict[k])
exit(0)





