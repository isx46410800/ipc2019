#!/usr/bin/python3
# -*- coding: utf-8 -*-
#08-sort-users.py [-s login|gid] file
#Carregar en una llista a memòria els usuaris provinents d'un fitxer tipus /etc/passwd, usant objectes UnixUser, i llistar-los.
#Ordenar el llistat (stdout) segons el criteri login o el criteri gid (estable).
#Ordenació estable dels objectes

import sys, argparse
parser = argparse.ArgumentParser(description="Iterar els usuaris del /etc/passwd", epilog="Fi del programa")
parser.add_argument("-s", "--sort", type=str, help="ordenacio per login o gid", metavar="ordre", dest="ordenacio", choices=["login","gid"])
parser.add_argument("fitxer",type=str, help="file /etc/passwd o stdin", metavar="file")
args=parser.parse_args()
# -------------------------------------------------------
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,liniaUser):
    "Constructor objectes UnixUser"
    campos=liniaUser.split(":")
    self.login=campos[0]
    self.passwd=campos[1]
    self.uid=int(campos[2])
    self.gid=int(campos[3])
    self.gecos=campos[4]
    self.home=campos[5]
    self.shell=campos[6][:-1]
  def show(self):
    "Mostra les dades de l'usuari"
    print("login: %s, passwd: %s, uid: %d, gid: %d, gecos: %s, home: %s, shell: %s" % (self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell))
  def __str__(self):
    "funció to_string"
    return "%s %s %d %d %s %s %s" % (self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)
# -------------------------------------------------------
#funcion para ordenar por campo1 login
def campo_login(user):
	return user.login
#funcion para ordenar por campo4 gid
def campo_gid(user):
	return user.gid
#abrimos fichero y metemos cada linia de user en la lista
fileIn=open(args.fitxer,"r")
userList=[]
for line in fileIn:
  user=UnixUser(line)
  userList.append(user)
fileIn.close()
#ordenamos segun se ponga -s login o gid
if args.ordenacio == "login":
	userList.sort(key=campo_login)
else:
	userList.sort(key=campo_gid)
#mostramos cada linea de la lista ordenada
for user in userList:
 print(user)
exit(0)


