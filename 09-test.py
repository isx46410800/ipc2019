# /usr/bin/python
#-*- coding: utf-8-*-
#
# sort-users [-s login|gid]  file
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------
import sys, argparse

#iniciamos el diccionario
groupsDict={}

#definimos argumentos
import sys, argparse
parser = argparse.ArgumentParser(description="Iterar els usuaris del /etc/passwd", epilog="Fi del programa")
parser.add_argument("-s", "--sort", type=str, help="ordenacio per login, gid o gname", metavar="ordre", dest="ordenacio", choices=["login","gid", "gname"])
parser.add_argument("-u", "--userFile", required=True, type=str, help="file /etc/passwd", metavar="userFile")
parser.add_argument("-g", "--groupFile", required=True, type=str, help="file /etc/passwd", metavar="groupFile")
args=parser.parse_args()
# -------------------------------------------------------
#CLASE PER A /ETC/PASSWD
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userLine):
    "Constructor objectes UnixUser"
    userField=userLine.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gname=""
    #buscamos en el dicc segun gid(key) y el campo gname(valor) si está:
    if self.gid in groupsDict:
		self.gname=groupsDict[self.gid].gname
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6][:-1]
  def show(self):
    "Mostra les dades de l'usuari"
    print(f"login: {self.login} uid: {self.uid} gid: {self.gid} gecos: {self.gecos} home: {self.home} shell: {self.shell}")
  def __str__(self):
    "functió to_string"
    return "%s %s %d %d %s %s %s" %(self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)
# -------------------------------------------------------
#CLASE PARA EL /ETC/GROUP
class UnixGroup():
  """Classe UnixUser: prototipus de /etc/group
  gname:passwd:gid:usersList"""
  def __init__(self,liniaGroup):
    "Constructor objectes UnixUser"
    campos=liniaGroup.split(":")
    self.gname=campos[0]
    self.passwd=campos[1]
    self.gid=int(campos[2])
    self.userslist=campos[3][:-1]
  def show(self):
    "Mostra les dades del group"
    print("gname: %s, passwd: %s, gid: %d, usersList: %s" % (self.gname, self.passwd, self.gid, self.userslist))
  def __str__(self):
    "funció to_string"
    return "%s %s %d %s" % (self.gname, self.passwd, self.gid, self.userslist)
# -------------------------------------------------------
#ORDENACIO
def sort_login(user):
  '''Comparador d'usuaris segons el login'''
  return user.login
  
def sort_gid(user):
  '''Comparador d'usuaris segons el gid'''
  return (user.gid, user.login)
def sort_gname(user):
  '''Comparador d'usuaris segons el gname'''
  return (user.gname, user.gid, user.login)
# -------------------------------------------------------
#OBRIM FITXER /etc/group
fileIn=open("group","r")
##recorre cada linia i afegeix al diccionari
for line in fileIn:
  oneGroup=UnixGroup(line)
  groupsDict[oneGroup.gid]=oneGroup
fileIn.close()
print(groupsDict)

# -------------------------------------------------------
#OBRIM FITXER /etc/passwd
fileIn=open("passwd","r")
userList=[]
for line in fileIn:
  oneUser=UnixUser(line)
  userList.append(oneUser)
fileIn.close()
print(userList[0])
exit(0)
# -------------------------------------------------------
#ordenamos segun se ponga -s login o gid o gname
if args.ordenacio == "login":
	userList.sort(key=campo_login)
elif args.ordenacio == "gid":
	userList.sort(key=campo_gid)
else:
	userList.sort(key=campo_gname)
# -------------------------------------------------------
#mostramos resultados	
for user in userList:
 print(user)
exit(0)

