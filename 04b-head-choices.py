#!/usr/bin/python3

#04-head-choices.py [-n 5|10|15]   -f filein
#Mostar les n primeres 5 o 10 o 15 (def 10)  de filein.
#Usar choices de argparse per definir un conjunt de valors vàlids.

import sys, argparse

parser = argparse.ArgumentParser(description="prog per mostrar les 5|10|15 primer linies de un fitxer", epilog="Programa terminat")
parser.add_argument("-n","--nlin", type=int, dest="nlin", help="Num de linies", default=10, choices=[5,10,15],)
#para hacer una opcion obligatoria: -f file (pero la variable la guardamos como fitxer)
parser.add_argument("-f", "--file", "--fitxer", required=True, type=str, help="fitxer a processar", metavar="file", dest="fitxer")
args=parser.parse_args()
print(args)

#abrimos fichero por stdin
MAXLIN=args.nlin
fileIn=open(args.fitxer,"r")
counter=0
for line in fileIn:
  counter+=1
  print(line)
  if counter==MAXLIN: break
fileIn.close()
exit(0)
