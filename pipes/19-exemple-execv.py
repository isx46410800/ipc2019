# /usr/bin/python
#-*- coding: utf-8-*-
#
# 19-exemple-execv.py
#Ídem anterior però ara el programa fill execula un “ls -la /”. Executa un nou
#procés carregat amb execv. Aprofitar per veure les fiferents variants de exec.
# -------------------------------------------
import sys, os
	
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
	print("Programa Pare", os.getpid(), pid)
	sys.exit(0)

print("Programa fill", os.getpid(), pid)
##con la v ponemos los args en una lista
#os.execv("/usr/bin/ls",["/usr/bin/ls","-ls","/"])
##con la l ponemos los argumentos uno a uno
#os.execl("/usr/bin/ls", "/usr/bin/ls","-ls","/")
##si ponemos la p no hace falta poner la ruta absoluta
#os.execlp("ls", "ls","-ls","/")
#os.execvp("ls", ["ls","-ls","/"])
#con la e se le ha de pasar el ENV, las otras lo heredan del padre
#os.execvp("/usr/bin/echo", ["/usr/bin/echo", "$HOSTNAME", "$edat"])
#os.execle("echo", "echo", "$HOSTNAME", "$edat", {"nom":"joan","edat":"18"})
#os.execv("/bin/bash",["/bin/bash", "/var/tmp/m06/ipc2019/show.sh"])
os.execle("/bin/bash","/bin/bash", "/var/tmp/m06/ipc2019/show.sh",{"nom":"joan","edat":"18"})

print("Hasta luego Lucas")
sys.exit(0)
