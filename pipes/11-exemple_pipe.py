#!/usr/bin/python3
# -*- coding: utf-8 -*-
#11-exemple_pipe.py

# -------------------------------------------------------
#librerias importadas
import sys, argparse
#para no utilizar subprocess.Popen y subprocess.PIPE, hacemos el from:
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description="Exemple popen")
parser.add_argument("ruta", type=str, help="directori a llistar")
args=parser.parse_args()
# -------------------------------------------------------
#definimos la orden a hacer y la variable(el dir a llistar)
command = ["ls", args.ruta]
#POPEN es el constructor que crea el pipe (la orden, la salida de la orden ha de ser la pipe)
pipeData = Popen(command, stdout=PIPE)
#queremos leer de la salida de la pipe, de lo que sale de la tuberia.IN: seria para escribir en la tuberia
for line in pipeData.stdout:
	print(line)

exit(0)

