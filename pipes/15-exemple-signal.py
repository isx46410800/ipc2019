# /usr/bin/python
#-*- coding: utf-8-*-
#
# 15-exemple-signal.py
#Programa d'exemple del funcionament de signal. Defineix handlers i els assigna als senyals
#sigalarm i sigterm. Finalitza automàticament en passar els segons de l'alarma definida.
# -------------------------------------------
import sys, os, signal
def myhandler(signum,frame):
	print("Signal handler called with signal: ", signum)
	print("Adiosssssssssss")
	sys.exit(1)

def mydeath(signum,frame):
	print("Signal handler called with signal: ", signum)
	print("no em dona la gana de morir!")

#crida la funcio signal de la libreria signal, del señal SIGALARM(14)
# a la funcio myhandler
signal.signal(signal.SIGALRM,myhandler) #14
signal.signal(signal.SIGUSR2,myhandler) #12
signal.signal(signal.SIGUSR1,mydeath) #10
signal.signal(signal.SIGTERM,signal.SIG_IGN) #15(terminar)-->ignorar
#señal cuando hacemos control + c
signal.signal(signal.SIGINT,signal.SIG_IGN)
#cuando pasa este tiempo de alarma, llama al SIGALRM(funcion myhandler llamará)
signal.alarm(60)
#mostrar el pid del programa
print(os.getpid())

while True:
	pass
signal.alarm(0)
sys.exit(0)
