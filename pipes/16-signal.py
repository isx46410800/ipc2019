# /usr/bin/python
#-*- coding: utf-8-*-
#
# 16-signal.py segons
#Deifineix alarma(n segons). siguser1 incrementa 1 minut, siguser2 decrementa1 minut, sighub reinicia el compte,
#sigterm mostra quants segons falten de alarma, no és interrumpible amb control c.
#Sigalarm  mostra el valor de upper (quants hem incrementat) i down (quants decrementat) i acaba.
# -------------------------------------------
import sys, os, signal, argparse
#definim els segons de l'alarma
parser = argparse.ArgumentParser(description="exercici d'alarmnes", epilog="Fi del programa")
parser.add_argument("segons", type=int, help="numero de segons", metavar="segons")
args=parser.parse_args()

#variable para utilizar independiente de cada funcion
global up
global down
up=0
down=0

#definim funcions:
def up_usr1(signum,frame):
	global up
	print("Signal handler called with signal: ", signum)
	up+=1
	print("Sumamos 60 segundos al tiempo de alarma")
	falta=signal.alarm(0)
	signal.alarm(falta+60)

def down_usr2(signum,frame):
	global down
	print("Signal handler called with signal: ", signum)
	down+=1
	print("Restamos 60 segundos al tiempo de alarma")
	falta=signal.alarm(0)
	if (falta-60)<0:
		print("No se puede restar")
		signal.alarm(falta)
	else:
		signal.alarm(falta-60)

def hup(signum,frame):
	print("Signal handler called with signal: ", signum)
	print("Reiniciamos el tiempo a %d segundos" %(args.segons))
	signal.alarm(args.segons)
	
def term(signum,frame):
	print("Signal handler called with signal: ", signum)
	falta=signal.alarm(0)
	signal.alarm(falta)
	print("Faltan %s segundos"% (falta))
	
def alrm(signum,frame):
	print("Signal handler called with signal: ", signum)
	print("Numero ups: %d Numero downs: %d" % (up,down))
	print("Se acabó!!")
	sys.exit(1)


#crida de funcions segons señal:
signal.signal(signal.SIGUSR1,up_usr1) #10
signal.signal(signal.SIGUSR2,down_usr2) #12
signal.signal(signal.SIGHUP,hup) #1 
signal.signal(signal.SIGTERM,term) #15(terminar)-->ignorar
signal.signal(signal.SIGINT,signal.SIG_IGN) #señal cuando hacemos control + c, ignorar
signal.signal(signal.SIGALRM,alrm) #14

#cuando pasa este tiempo de alarma
signal.alarm(args.segons)
#mostrar el pid del programa
print(os.getpid())

while True:
	pass
signal.alarm(0)
sys.exit(0)
