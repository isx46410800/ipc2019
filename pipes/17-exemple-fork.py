# /usr/bin/python
#-*- coding: utf-8-*-
#
# 17-exemple-fork.py
#Exemple bàsic fork amb program apare que llança programa fill (un while infinit).
#Observar els PID i la lògica del fluxe de funcionament.
# -------------------------------------------
import sys, os

print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())
#fork nos bifurca y nos retorna 0 o diferente de 0. pid=num proceso hijo
pid=os.fork()
#si es diferente, espera a que acabe el hijo y te dice que es el padre
if pid !=0:
	os.wait()
	print("Programa Pare", os.getpid(), pid)
#sino(es 0), es el hijo
else:
	print("Programa fill", os.getpid(), pid)

print("Hasta luego Lucas")
sys.exit(0)
