#!/usr/bin/python3
# -*- coding: utf-8 -*-
#12-popen_sql.py

# -------------------------------------------------------
#librerias importadas
import sys, argparse
from subprocess import Popen, PIPE

# -------------------------------------------------------
#definimos la orden a hacer 
command = [" psql -qtA -F',' -h 172.17.0.2 -U postgres training -c \"select * from clientes; \""]
#POPEN es el constructor que crea el pipe (la orden, la salida de la orden ha de ser la pipe)
#SHELL TRUE: es como si lo ejecutaras como el /bin/sh y arguments
pipeData = Popen(command, shell=True, stdout=PIPE)
#queremos leer de la salida de la pipe, de lo que sale de la tuberia.IN: seria para escribir en la tuberia
for line in pipeData.stdout:
	print(line.decode("utf-8"), end="")

exit(0)

