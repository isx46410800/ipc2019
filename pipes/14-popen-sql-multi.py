# /usr/bin/python
#-*- coding: utf-8-*-
#
# 14-popen-sql-multi.py  -d database   [-c numclie]...
# Atacar la database indicada, llistar cada un dels registres del codi indicat.
# Explicacio dels diàlegs, un read o readlines es queda encallat at infinitum
# quan el subprocess no finalitza. Cal llegir exactament les n linies de resposta
# (dues si ok, una si error), pero no infinites.
# Exemple amb wc < file i wc de stdin.
# Atenció: no programar el cas desfaborable de posar num_clie que no existeixen,
# això implica buscar un mecanisme de diàleg tipus “canvi!”... com amb els walki-talki.
# Hem après a:

# posar el /n al final per que faci la acció.
# si el popen finalitza és com si hi ha el ^d al final de fi de fluxe i podem llegir les n línies que retorni.
# si el popem és interactiu i no finalitza cal saber a priori quantes línies llegir o es quedarà ‘enganxat’
# -------------------------------------------
import sys
from subprocess import Popen, PIPE
import argparse
parser = argparse.ArgumentParser(description='Consulta SQL interactiva')
parser.add_argument("-c", type=str, help="oficina a consultar", metavar="numOficina", dest="listaOficinas",action="append")
args = parser.parse_args()

#obre un flutxe
cmd = "psql -qtA -F',' -h 172.17.0.2 -U postgres training"
pipeData = Popen(cmd, shell = True, stdin=PIPE, stdout=PIPE, stderr=PIPE, bufsize=0, universal_newlines=True)

for oficina in args.listaOficinas:
	consulta = "select * from oficinas where oficina=%s;" %(oficina)
	pipeData.stdin.write(consulta+"\n")
	print(pipeData.stdout.readline())

pipeData.stdin.write("\q\n")
sys.exit(0)


