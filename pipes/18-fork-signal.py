# /usr/bin/python
#-*- coding: utf-8-*-
#
# 18-fork-signal.py
#el pare s'engega, te un fill i es mor
#el fill es un programa que es queda en un bucle infinit
#Usant el programa d'exemple fork fer que el procés fill (un while infinit) es
#governi amb senyals. Amb siguser1 mostra "hola radiola" i amb sigusr2 mostra
#"adeu andreu" i finalitza.
# -------------------------------------------
import sys, os, signal
#definim funcions:
def sigusr1(signum,frame):
	print("Hola radiola")

def sigusr2(signum,frame):
	print("Adeu andreu!")
	sys.exit(0)
	
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork()
if pid !=0:
	print("Programa Pare", os.getpid(), pid)
	sys.exit(0)
else:
	print("Programa fill", os.getpid(), pid)
	while True:
		pass

#señales
signal.signal(signal.SIGUSR1,sigusr1) #10
signal.signal(signal.SIGUSR2,sigusr2) #12

signal.alarm(0)
sys.exit(0)




# ##comprovacion
# [isx46410800@i05 pipes]$ python3 18-fork-signal.py 
# Hola, començament del programa principal
# PID pare:  7994
# Programa Pare 7994 7995
# Programa fill 7995 0
# [isx46410800@i05 pipes]$ ps 7995
  # PID TTY      STAT   TIME COMMAND
 # 7995 pts/3    R      0:24 python3 18-fork-signal.py
# [isx46410800@i05 pipes]$ kill -10 7995
# Hola radiola
# [isx46410800@i05 pipes]$ kill -12 7995
# Adeu andreu!
# [isx46410800@i05 pipes]$ ps 7995
  # PID TTY      STAT   TIME COMMAND
