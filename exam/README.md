# EXAMEN M06 - UF2 - Miguel Amorós

## Server

+ Primero con argparse hemos creado la forma a la que llamar el programa:
```
server01.py [-d|--debug] [-p|--port]
```

+ Después hemos hecho un fork para que se quede trabajando en segundo plano para poder gestionarlo con señales.

+ Hemos creado con funciones 3 tipos de señales(SIGUSR1, SIGUSR2, SIGTERM) y la hemos asignado:
  + SIGUSR1: lista todas las conexiones y finaliza.
  + SIGUSR2: indica el numero total de conexiones recibidas y finaliza.
  + SIGTERM: finaliza todo mostrando el total de conexiones y su listado.
  

+ Hemos creado un while true con la conexión para que se pueda hacer varias conexiones.

## Client

+ Primero con argparse hemos creado la forma a la que llamar el programa:
```
client01.py [-p|--port] server
```

+ Después hemos creado un while true para poder realizar varias consultas hasta que haya un EOF.

+ Creamos una serie de comandos para que según se le indique, el servidor nos muestre la info oportuna:
  + processos
  + ports
  + whoareyou

## AMAZON

+ Hemos creado una nueva instancia en AMAZON:

+ Hemos abierto los puertos SSH y el puerto 44444 para poder conectarnos al servidor metido allí.

+ ip: 3.9.176.81
