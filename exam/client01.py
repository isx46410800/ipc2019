# /usr/bin/python
#-*- coding: utf-8-*-
# Miguel Amoros - M06 - isx46410800
# Client01.py [-p|--port] server
# -------------------------------------
import sys,socket,argparse
from subprocess import Popen, PIPE
#formamos con argparse los parametros
parser = argparse.ArgumentParser(description="""client""")
parser.add_argument("-p","--port",type=int, default=44444)
parser.add_argument("server",type=str)
args=parser.parse_args()
#la conexion de host,port la formamos con la variable de las args
HOST = args.server
PORT = args.port
MYEOF = bytes(chr(4), 'utf-8')
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((HOST, PORT))

while True:
    command = input("ordre> ")
    if not command: break
    s.sendall(command.encode("utf-8"))
    while True:
        data=s.recv(1024)
        if data[-1:]== MYEOF:
            print(repr(data[:-1]))
            break
        print(repr(data))
s.close()
sys.exit(0)
