# /usr/bin/python3
#-*- coding: utf-8-*-
# Miguel Amoros - M06 - isx46410800
# server01.py [-d|--debug] [-p|--port]
# -------------------------------------
import sys,socket,os,signal,argparse, time
from subprocess import Popen, PIPE

#formamos con argparse los parametros
parser = argparse.ArgumentParser(description="""Daemon server""")
parser.add_argument("-d","--debug",type=int, default=1)
parser.add_argument("-p","--port",type=int, default=44444)
args=parser.parse_args()

#la conexion de host,port la formamos con la variable de las args
llistaPeers=[]
HOST = ''
PORT = args.port
DEBUG = args.debug
MYEOF = bytes(chr(4), 'utf-8')

#funciones de las señales
##lista de peers y tanca
def mysigusr1(signum,frame):
  print ("Signal handler called with signal:", signum)
  print (llistaPeers)
  sys.exit(0)
##count de listapeers y tanca
def mysigusr2(signum,frame):
  print ("Signal handler called with signal:", signum)
  print (len(llistaPeers))
  sys.exit(0)
##lista de peers, count de listapeers y tanca
def mysigterm(signum,frame):
  print ("Signal handler called with signal:", signum)
  print (llistaPeers, len(llistaPeers))
  sys.exit(0)

#se hace un fork del proceso padre(que muere) y se gobierna con señales
pid=os.fork()
if pid !=0:
  print ("Engegat el server PS:", pid)
  sys.exit(0)

signal.signal(signal.SIGUSR1,mysigusr1) #10
signal.signal(signal.SIGUSR2,mysigusr2) #12
signal.signal(signal.SIGTERM,mysigterm) #15

#escuchamos la conexion del cliente
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)

#recibe las conexiones del cliente
while True: #para cada cliente
  conn, addr = s.accept()
  print ("Connected by", addr)
  llistaPeers.append(addr)
  while True:
      data = conn.recv(1024)
      if not data: break
      if data == b'processos\n':
          cmd = 'ps ax'
      elif data == b'ports\n':
          cmd = 'netstat -puta'
      elif data == b'whoareyou\n':
          cmd = 'uname -a'
      else:
          cmd = 'uname -a'
      pipeData = Popen(cmd, shell=True, bufsize=0, universal_newlines=True, stdout=PIPE, stderr=PIPE)
      for line in pipeData.stdout:
        if DEBUG:
            print("Enviando debugs: %s" % (line))
        conn.sendall(line.encode("utf-8"))
      for line in pipeData.stderr:
        if DEBUG:
            print("Enviando debugs: %s" % (line))
        conn.sendall(line.encode("utf-8"))
      conn.sendall(MYEOF)
  conn.close()
s.close()
sys.exit(0)
