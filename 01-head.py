#!/usr/bin/env python
# -*- coding:utf-8-*-
import sys
MAXLIN=10
fileIn=sys.stdin
if len(sys.argv)==2:
  fileIn=open(sys.argv[1],"r")
counter=0
for line in fileIn:
  counter+=1
  print (line)
  if counter==MAXLIN: break
fileIn.close()
exit(0)

