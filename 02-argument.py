#!/usr/bin/python3
# -*- coding:utf-8-*-
#------------------------
#importem llibreria
import argparse

#creem l'objecte
parser = argparse.ArgumentParser(description="prog exemple de processar arguments", prog="02-arguments.py", epilog="hasta luego lucas!")
#passem el argument(dest,fit,filein, son variables on es magatzema el contingut)
parser.add_argument("-e","--edat", type=int, dest="useredat", help="edat a processar")
parser.add_argument("-f","--fit", type=str, dest="fitxer", help="fitxer a processar", metavar="fileIn")
#es crea un diccionario con los argumentos que le pasamos en este metodo parse_args
args=parser.parse_args()
#printem resultats
print(parser)
print(args)
#com args es un diccionario, solo variables de dentro de parse, no globales, las pasamos como diccionario
print(args.fitxer, args.useredat)
exit(0)
