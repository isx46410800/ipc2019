# /usr/bin/python
#-*- coding: utf-8-*-
#
# count-by-group.py [-s gid|gname|nusers] -u users -g groups
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
# -------------------------------------------------------
#Definimos diccionario para el gname
groupsDict={}

# -------------------------------------------------------
import sys, argparse
parser = argparse.ArgumentParser(description="Iterar els usuaris del /etc/passwd", epilog="Fi del programa")
parser.add_argument("-s", "--sort", type=str, help="ordenacio per login, gid o gname", metavar="ordre", dest="ordenacio", choices=["nusers","gid", "gname"])
parser.add_argument("-u", "--userFile", required=True, type=str, help="file /etc/passwd", metavar="userFile")
parser.add_argument("-g", "--groupFile", required=True, type=str, help="file /etc/passwd", metavar="groupFile")
args=parser.parse_args()
# -------------------------------------------------------
#CLASE PARA EL /ETC/PASSWD
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,userLine):
    "Constructor objectes UnixUser"
    userField=userLine.split(":")
    self.login=userField[0]
    self.passwd=userField[1]
    self.uid=int(userField[2])
    self.gid=int(userField[3])
    self.gname=""
    if self.gid in groupsDict:
      self.gname=groupsDict[self.gid].gname
    self.gecos=userField[4]
    self.home=userField[5]
    self.shell=userField[6]
  def show(self):
    "Mostra les dades de l'usuari"
    print("login: %s, passwd: %s, uid: %d, gid: %d, gecos: %s, home: %s, shell: %s" % (self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell))
  def __str__(self):
    "funció to_string"
    return "%s %s %d %d %s %s %s" % (self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)
# -------------------------------------------------------
#CLASE PARA EL /ETC/GROUP
class UnixGroup():
  """Classe UnixGroup: prototipus de /etc/group
  gname_passwd:gid:listUsers"""
  def __init__(self,groupLine):
    "Constructor objectes UnixGroup"
    groupField = groupLine.split(":")
    self.gname = groupField[0]
    self.passwd = groupField[1]
    self.gid = int(groupField[2])
    self.userListStr = groupField[3]
    self.userList=[]
    if self.userListStr[:-1]:
      self.userList = self.userListStr[:-1].split(",")
  def __str__(self):
    "functió to_string d'un objecte UnixGroup"
    return "%s %d %s" % (self.gname, int(self.gid), self.userList)
# -------------------------------------------------------
#------------------------------------------------------- 
#abrimos fichero /etc/group
groupFileIn=open(args.groupFile,"r")
for line in groupFileIn:
  group=UnixGroup(line)
  groupsDict[group.gid]=group
groupFileIn.close()
# --------------------------------------------------------
#abrimos fichero /etc/passwd y metemos cada linia de user en la lista
userFileIn=open(args.userFile,"r")
userList=[]
for line in userFileIn:
  user=UnixUser(line)
  userList.append(user)
  #si está ese gid en el DICC, añadimos el user sino está en el campo4 de /etc/group
  if user.gid in groupsDict:
    if user.login not in groupsDict[user.gid].userList:
	  groupsDict[user.gid].userList.append(user.login)
userFileIn.close()
# -------------------------------------------------------
#ordenamos segun se ponga -s login o gid o nusers
index=[]
if args.ordenacio == "gid":
	index = [ k for k in groupsDict ]
elif args.ordenacio == "gname":
	index = [ (groupsDict[k].gname,k) for k in groupsDict ]
elif args.ordenacio == "nusers":
	index = [ (len(groupsDict[k].userList),k) for k in groupsDict ]
##odenamos estos index para tener la lista ordenada segun gid, gname o nusers
index.sort()
# -------------------------------------------------------	
#mostramos cada linea del groups segun su ordenacion
if args.ordenacio=="gid":
  for k in index:
   print(groupsDict[k])
else:
  for g,k in index:
   print groupsDict[k]
exit(0)
