#!/usr/bin/python3
#06-exemple-objectes
#EXEMPLE DE PROGRAMACIO OBJECTES AMB PYTHON

#vaig a construir una clase(nom comença en mayuscula)
class UnixUser():
	"""Classe UnixUser: prototipus de
	 /etc/passwd
	 login:passwd:uid:gid:gecos:home:shell"""
	 #creem un constructor, self es per a mi mateix i les variables que li indiquem
	 #init de inicialitzador del constructor
	 def __init__(self, l, i, g):
		"Constructor objectes UnixUser"
		self.login=l
		self.uid=i
		self.gid=g
	def show(self):
		"Mostrar les dades de l'usuari"
		print("login: %s uid: %d gid: %d" % (self.login, self.uid, self.gid))
	def sumaun(self):
		self.uid+=1
	#__x__ son metodos implicitos
	def __str__(self):
		"Funcio per retornar un string del objecte"
		return "%s %d %d" %(self.login, self.uid, self.gid)
		
exit(0)

#exemple
#user1=UnixUser("marta",1000,1000)
# >>> print(user1)
# <__main__.UnixUser object at 0x7fa90b5d7b70>
# >>> user1
# <__main__.UnixUser object at 0x7fa90b5d7b70>
# >>> user1.sumaun()
# >>> user1.show()
# login: marta uid: 1001 gid: 1000
# >>> 



