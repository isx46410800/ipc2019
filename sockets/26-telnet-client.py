# /usr/bin/python
#-*- coding: utf-8-*-
# 26 - telnet client
# -------------------------------------
import sys,socket,argparse
from subprocess import Popen, PIPE
#formamos con argparse los parametros
parser = argparse.ArgumentParser(description="""Telnet client""")
parser.add_argument("-p","--port",type=int, required=True, default=50001)
parser.add_argument("-s","--server",type=str, required=True)
args=parser.parse_args()
#la conexion de host,port la formamos con la variable de las args
HOST = args.server
PORT = args.port
MYEOF = bytes(chr(4), 'utf-8')
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))

#hacemos una conexion con entrada telnet>
while True:
	command = input("telnet> ")
	if not command: break
	s.sendall(command.encode("utf-8"))
	while True:
		data=s.recv(1024)
		if data[-1:] == MYEOF: 
			print(repr(data[:-1]))
			break
		print(repr(data))
s.close()
sys.exit(0)

