#!/usr/bin/python
#-*- coding: utf-8-*-
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2018-2019
# Echo server multiple-connexions
# -----------------------------------------------------------------

import socket, sys, select, os, signal, argparse, time
from subprocess import Popen, PIPE

#la conexion de host,port la formamos con la variable de las args
HOST = ''
PORT = 50001
MYEOF = bytes(chr(4), 'utf-8')

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
print(os.getpid())
conns=[s]
while True:
    actius,x,y = select.select(conns,[],[])
    for actual in actius:
        if actual == s:
            conn, addr = s.accept()
            print('Connected by', addr)
            conns.append(conn)
        else:
            data = actual.recv(1024)
            if not data:
                sys.stdout.write("Client finalitzat: %s \n" % (actual))
                actual.close()
                conns.remove(actual)
            else:
				command=data
				if not command: break
				pipeData = Popen(command, shell=True, bufsize=0, universal_newlines=True, stdout=PIPE, stderr=PIPE)
				for line in pipeData.stdout:
					conn.sendall(line.encode("utf-8"))
				for line in pipeData.stderr:
					conn.sendall(line.encode("utf-8"))
				conn.sendall(MYEOF)
				
s.close()
sys.exit(0)	
                
