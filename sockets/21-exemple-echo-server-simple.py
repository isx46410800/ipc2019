# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket
HOST = ''
PORT = 50001
#crea un socket de Internet de tipos TCP
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#el host(ip) y el puerto por el que estamos escuchando
s.bind((HOST,PORT))
#Activamos que escuche
s.listen(1)
#Se queda enganchado hasta el finito, hasta que digamos hasta cuando(accept)
#retorna una tupla(objeto con el objecte connexio i la adreça del client)
conn, addr = s.accept()
print("Connected by", addr)
#escucha cosas y envia data
while True:
  data = conn.recv(1024)
  #NO DIU quan no hi ha mes dades. Diu que m'han penjat el telefon
  #La conexion se ha cerrado por el otro extremo
  if not data: break #valdrá NONE
  conn.send(data)
conn.close()
sys.exit(0)


