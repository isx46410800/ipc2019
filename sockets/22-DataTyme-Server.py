# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket
from subprocess import Popen, PIPE
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)
conn, addr = s.accept()
print("Connected by", addr)

#queremos mandar DATE a traves de un PIPE
command = ["date"]
pipeData = Popen(command, stdout=PIPE)
#llegeixo el contingunt del pipe i l'envio per la xarxa
for line in pipeData.stdout:
	conn.send(line)
#el servidor incondicionalment, tanca
conn.close()
sys.exit(0)	


