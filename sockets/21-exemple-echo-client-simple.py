# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
#esto sirve para que se pueda reutilizar el puerto y no se quede ocupado un tiempo
#s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#me conecto
s.connect((HOST, PORT))
#en bytes strings
s.send(b'Hello, world')
#recibo cosas
data = s.recv(1024)
#cierro conexion y devuelve un none
s.close()
print('Received', repr(data))
sys.exit(0)


