#!/usr/bin/python
#-*- coding: utf-8-*-
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2018-2019
# Echo server multiple-connexions
# -----------------------------------------------------------------

import socket, sys, select, os

HOST = ''                 
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
print(os.getpid())
conns=[s]
while True:
    actius,x,y = select.select(conns,[],[])
    for actual in actius:
        if actual == s:
            conn, addr = s.accept()
            print('Connected by', addr)
            conns.append(conn)
        else:
            data = actual.recv(1024)
            if not data:
                sys.stdout.write("Client finalitzat: %s \n" % (actual))
                actual.close()
                conns.remove(actual)
            else:
                actual.sendall(data)
                actual.sendall(bytes(chr(4), 'utf-8'),socket.MSG_DONTWAIT)
s.close()
sys.exit(0)


#COMPROBACION
# [isx46410800@localhost sockets]$ python3 27-echo-sever-multi.py 
# 5201
# Connected by ('127.0.0.1', 46308)
# Connected by ('127.0.0.1', 46312)
# Connected by ('127.0.0.1', 46318)

# [isx46410800@localhost sockets]$ ncat localhost 50002
# [isx46410800@localhost sockets]$ telnet localhost 50002
# [isx46410800@localhost sockets]$ python 22-DataTyme-Client.py 

#Client finalitzat: <socket.socket fd=6, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=0, laddr=('127.0.0.1', 50002), raddr=('127.0.0.1', 46318)> 
#Client finalitzat: <socket.socket fd=5, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=0, laddr=('127.0.0.1', 50002), raddr=('127.0.0.1', 46312)> 
#Client finalitzat: <socket.socket fd=4, family=AddressFamily.AF_INET, type=SocketKind.SOCK_STREAM, proto=0, laddr=('127.0.0.1', 50002), raddr=('127.0.0.1', 46308)> 

