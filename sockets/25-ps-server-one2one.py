# /usr/bin/python
#-*- coding: utf-8-*-
# 25 - ps-one2one
# -------------------------------------
import sys,socket,os,signal,argparse, time
from subprocess import Popen, PIPE
import sys,socket,argparse
from subprocess import Popen, PIPE
#formamos con argparse los parametros
parser = argparse.ArgumentParser(description="""PS server""")
parser.add_argument("-p","--port",type=int, default=50003)
args=parser.parse_args()
#la conexion de host,port la formamos con la variable de las args
llistaPeers=[]
HOST = ''
PORT = args.port

#funciones de las señales
##lista de peers y tanca
def mysigusr1(signum,frame):
  print "Signal handler called with signal:", signum
  print llistaPeers
  sys.exit(0)
##count de listapeers y tanca  
def mysigusr2(signum,frame):
  print "Signal handler called with signal:", signum
  print len(llistaPeers)
  sys.exit(0)
##lista de peers, count de listapeers y tanca
def mysigterm(signum,frame):
  print "Signal handler called with signal:", signum
  print llistaPeers, len(llistaPeers)
  sys.exit(0)
 
#se hace un fork del proceso padre(que muere) y se gobierna con señales  
pid=os.fork()
if pid !=0:
  print "Engegat el server PS:", pid
  sys.exit(0)
  
signal.signal(signal.SIGUSR1,mysigusr1)
signal.signal(signal.SIGUSR2,mysigusr2)
signal.signal(signal.SIGTERM,mysigterm)

#escuchamos la conexion del cliente
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)

#recibe las conexiones del cliente
while True: #para cada cliente
  conn, addr = s.accept() #aceptamos conexion
  print "Connected by", addr
  llistaPeers.append(addr) #añade ip/port a la lista
  fileName="/tmp/%s-%s.log" % (addr[0],addr[1]) #fichero que creamos
  fileLog=open(fileName,"w") #abrimos fichero
  while True: #mientras recibe datas pipe OK,y cuando no, tanca y guarda file
	data = conn.recv(1024)
	if not data: break
	fileLog.write(str(data))
  conn.close()
  fileLog.close()
sys.exit(0)
