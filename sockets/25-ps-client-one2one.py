# /usr/bin/python
#-*- coding: utf-8-*-
# 25-psOne2one
# -------------------------------------
import sys,socket,argparse
from subprocess import Popen, PIPE
#formamos con argparse los parametros
parser = argparse.ArgumentParser(description="""PS client""")
parser.add_argument("-p","--port",type=int, default=50003)
parser.add_argument("server",type=str)
args=parser.parse_args()
#la conexion de host,port la formamos con la variable de las args
HOST = args.server
PORT = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
#hacemos una conexion que desde el cliente envie un popen al server
#de un ps ax
command = ["ps ax"]
pipeData = Popen(command,shell=True,stdout=PIPE)
for line in pipeData.stdout:
	s.send(line)
s.close()
sys.exit(0)


