# /usr/bin/python
#-*- coding: utf-8-*-
# 26 - telnet server
# -------------------------------------
import sys,socket,os,signal,argparse, time
from subprocess import Popen, PIPE

#formamos con argparse los parametros
parser = argparse.ArgumentParser(description="""Telnet server""")
parser.add_argument("-p","--port",type=int, default=50001)
parser.add_argument("-d","--debug",type=int, default=1)
args=parser.parse_args()

#la conexion de host,port la formamos con la variable de las args
HOST = ''
PORT = args.port
DEBUG = args.debug
MYEOF = bytes(chr(4), 'utf-8')
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)

#recibe la conexion y el comand es la xixa que le envia el cliente
#con la xixa se monta un popen y se envia de nuevo al cliente para
#que te pueda volver a poner otras ordenes
while True:
  conn, addr = s.accept()
  print ("Connected by", addr)
  while True:
	  command = conn.recv(1024)
	  if not command: break
	  pipeData = Popen(command, shell=True, bufsize=0, universal_newlines=True, stdout=PIPE, stderr=PIPE)
	  for line in pipeData.stdout:
		  if DEBUG:
			  print("Enviando: %s" %(line))
		  conn.sendall(line.encode("utf-8"))
	  for line in pipeData.stderr:
		  if DEBUG:
			  print("Enviando: %s" %(line))
		  conn.sendall(line.encode("utf-8"))
	  conn.sendall(MYEOF)
  conn.close()
s.close()
sys.exit(0)


