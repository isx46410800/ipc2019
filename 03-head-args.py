#!/usr/bin/python3
#03-head-args.py [-n nlin] [-f filein]
#Mostar les n primeres línies (o 10)  de filein (o stdin). Definir els dos paràmetres opcionals i les variables on desar-los. Usar un str default de “/dev/stdin” com a nom de fitxer per defecte, simplifica el codi, tenim sempre un string.

import sys, argparse

parser = argparse.ArgumentParser(description="prog per mostrar les n primer linies de un fileIn", prog="03-head-args.py", epilog="Programa terminat")
parser.add_argument("-n","--nlin", type=int, dest="nlin", help="Num de linies", default=10)
parser.add_argument("-f","--fit", type=str, dest="fitxer", help="fitxer a processar", metavar="fileIn", default="/dev/stdin")
args=parser.parse_args()
print(args)

#abrimos fichero por stdin
MAXLIN=args.nlin
fileIn=open(args.fitxer,"r")
counter=0
for line in fileIn:
  counter+=1
  print(line)
  if counter==MAXLIN: break
fileIn.close()
exit(0)

