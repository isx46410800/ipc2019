#!/usr/bin/python3
# -*- coding: utf-8 -*-
#09-gname-users.py   [-s login|gid|gname]  -u fileusers -g fileGroup
#Carregar en una llista a memòria els usuaris provinents d'un fitxer tipus /etc/passwd, usant objectes UnixUser, i llistar-los. Ordenar el llistat
#(stdout) segons el criteri login o el criteri gid o el gname.
#Requeriments: primer carregar a un diccionari totes les dades de tots els grups. Després carregar a una llista totes les dades dels usuaris.
#Finalment ordenar i llistar.
# -------------------------------------------------------
#librerias importadas
import sys, argparse
parser = argparse.ArgumentParser(description="Iterar els usuaris del /etc/passwd", epilog="Fi del programa")
parser.add_argument("-s", "--sort", type=str, help="ordenacio per login, gid o gname", metavar="ordre", dest="ordenacio", choices=["login","gid", "gname"])
parser.add_argument("-u", "--userFile", required=True, type=str, help="file /etc/passwd", metavar="userFile")
parser.add_argument("-g", "--groupFile", required=True, type=str, help="file /etc/passwd", metavar="groupFile")
args=parser.parse_args()
# -------------------------------------------------------
#Definimos diccionario para el ordenar por gname
groupsDict={}

# -------------------------------------------------------
#CLASE PARA EL /ETC/PASSWD
class UnixUser():
  """Classe UnixUser: prototipus de /etc/passwd
  login:passwd:uid:gid:gecos:home:shell"""
  def __init__(self,liniaUser):
    "Constructor objectes UnixUser"
    campos=liniaUser.split(":")
    self.login=campos[0]
    self.passwd=campos[1]
    self.uid=int(campos[2])
    self.gid=int(campos[3])
    #buscamos en el dicc segun gid(key) y el campo gname(valor) si está:
    self.gname=""
    if self.gid in groupsDict:
      self.gname=groupsDict[self.gid].gname
    self.gecos=campos[4]
    self.home=campos[5]
    self.shell=campos[6][:-1]
  def show(self):
    "Mostra les dades de l'usuari"
    print("login: %s, passwd: %s, uid: %d, gid: %d, gecos: %s, home: %s, shell: %s" % (self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell))
  def __str__(self):
    "funció to_string"
    return "%s %s %d %d %s %s %s" % (self.login, self.passwd, self.uid, self.gid, self.gecos, self.home, self.shell)
 
#CLASE PARA EL /ETC/GROUP
class UnixGroup():
  """Classe UnixUser: prototipus de /etc/group
  gname:passwd:gid:usersList"""
  def __init__(self,liniaGroup):
    "Constructor objectes UnixUser"
    campos=liniaGroup.split(":")
    self.gname=campos[0]
    self.passwd=campos[1]
    self.gid=int(campos[2])
    self.userslist=campos[3][:-1]
  def show(self):
    "Mostra les dades del group"
    print("gname: %s, passwd: %s, gid: %d, usersList: %s" % (self.gname, self.passwd, self.gid, self.userslist))
  def __str__(self):
    "funció to_string"
    return "%s %s %d %s" % (self.gname, self.passwd, self.gid, self.userslist)
# -------------------------------------------------------
#ORDENACION
#por login
def sort_login(user):
  '''Comparador d'usuaris segons el login'''
  return user.login
#por gid
def sort_gid(user):
  '''Comparador d'usuaris segons el gid'''
  return (user.gid, user.login)
#por gname
def sort_gname(user):
  '''Comparador d'usuaris segons el gname'''
  return (user.gname, user.gid, user.login)
#------------------------------------------------------- 
#abrimos fichero /etc/group y añadimos clave gid y valores de lineGroup
groupFileIn=open(args.groupFile,"r")
for line in groupFileIn:
  group=UnixGroup(line)
  groupsDict[group.gid]=group
groupFileIn.close()
# -------------------------------------------------------
#abrimos fichero /etc/passwd y metemos cada linia de user en la lista
userFileIn=open(args.userFile,"r")
userList=[]
for line in userFileIn:
  user=UnixUser(line)
  userList.append(user)
userFileIn.close()
# -------------------------------------------------------
#ordenamos segun se ponga -s login o gid o gname
if args.ordenacio == "login":
	userList.sort(key=sort_login)
elif args.ordenacio == "gid":
	userList.sort(key=sort_gid)
else:
	userList.sort(key=sort_gname)
# -------------------------------------------------------	
#mostramos cada linea de la lista ordenada
for user in userList:
 print(user)
exit(0)


