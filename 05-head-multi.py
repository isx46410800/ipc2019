#!/usr/bin/python3

#05-head-multi.py [-n 5|10|15]   [-f filein]...
#Mostar les n primeres 5 o 10 o 15 (def 10)  de filein.
#Processa múltiples files indicats per -f file, -f file, etc.
#Ampliació: [-v][--verbose] boleà que si hi és mostra una capçalera de llistat amb el nom del fitxer a llistar.

import sys, argparse

parser = argparse.ArgumentParser(description="prog per mostrar les 5|10|15 primer linies de un fitxer", epilog="Programa terminat")
parser.add_argument("-n","--nlin", type=int, dest="nlin", help="Num de linies", default=10, choices=[5,10,15],)
#para crear una lista de argumentos en una opcion se utiliza action=append
parser.add_argument("-f", "--fit", type=str, help="fitxer a processar", metavar="file", dest="listaFitxers", action="append")
args=parser.parse_args()
print(args)

#numero de lineas maximas
MAXLIN=args.nlin

#funcion head 
def head_function(fitxer):
	fileIn=open(fitxer,"r")
	counter=0
	for line in fileIn:
		counter+=1
		print(line)
		if counter==MAXLIN: break
	fileIn.close()

#para cada fichero de la lista de ficheros de arg_argument opcion -f, hacemos el head segun las lineas a leer:
for fichero in args.listaFitxers:
	print(fichero)
	head_function(fichero)
	
exit(0)
